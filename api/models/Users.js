/**
* Users.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
      user_id: { type: 'integer', autoIncrement: true, primaryKey: true, size: 11, columnName: 'user_id' },
      firstName: { type: 'string', size: 32, columnName: 'first_name' },
      lastName: { type: 'string', size: 32, columnName: 'last_name' },
      email: { type: 'string', size: 128, columnName: 'email' },
      username: { type: 'string', size: 40, columnName: 'username' },
      password: { type: 'string', size: 100, columnName: 'password' },
      createdAt: { type: 'datetime', columnName: 'created_at' },
      modifiedAt: { type: 'datetime', columnName: 'modified_at' },
      logTime: { type: 'datetime', columnName: 'log_time' },
      provider: { type: 'string', enum: ['local', 'google', 'facebook', 'twitter'], columnName: 'provider' }
  }
};

